package misc;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CurrencyConverter {
	
	private URL xeURL;
	private String pageContent;
	private static final int TIME_OUT = 5000;
	private static final String USER_AGENT = "Chrome";

	public CurrencyConverter()
	{
		try {
			xeURL = new URL("http://www.xe.com");
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public float getUSDAUDRate()
	{
		float exRate = 0;
		pageContent = getPageContent();		
		Scanner scanner = new Scanner(pageContent);
		String line = "";
		
		while (scanner.hasNextLine() == true)
		{
			line = scanner.nextLine();
			
			if (line.contains("AUD,USD,1,5") == true)
			{
//				System.out.println(line);
				String rate = extractContent(line, "AUD,USD,1,5\'>(.*?)</a>");
//				System.out.println("rate: " + rate);
				exRate = Float.parseFloat(rate);
			}
		}
		
		
		return exRate;
	}
	
	private String getPageContent() {
		String contents = "";
		String line;
		BufferedReader br;
		
		try
		{
			
//			System.out.println(url);
			HttpURLConnection httpConn = connectURL(xeURL);
			HttpURLConnection.setFollowRedirects(true);
			br = new BufferedReader(new InputStreamReader(httpConn.getInputStream()));
			
			while ((line = br.readLine()) != null)
			{
				contents += line;
				contents += "\n";
			}
			
			br.close();
			httpConn.disconnect();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		
		return contents;
	}
	
	private String extractContent(String line, String regex)
	{
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(line);
		String price = "";
		
		while (matcher.find() == true)
		{
			price = matcher.group(1);
		}
		
		return price;
	}
	
	private HttpURLConnection connectURL(URL url) {
		HttpURLConnection httpConn = null;
		try {
			httpConn = (HttpURLConnection) url.openConnection();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		httpConn.setConnectTimeout((int) TIME_OUT);
		httpConn.addRequestProperty("User-Agent", USER_AGENT);
		httpConn.setInstanceFollowRedirects(true);
		
		return httpConn;
	}
}
