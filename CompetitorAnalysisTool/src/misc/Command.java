package misc;

import gui.RetailGui;
import gui.TableRenderer;

import java.awt.Component;
import java.awt.Cursor;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.RowSorter;
import javax.swing.SortOrder;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

import plugins.CompetitorAnalysis;
import resources.Product;

public class Command extends SwingWorker<Object, Object>
{
	private PluginFinder oPluginFinder;
	private CompetitorAnalysis clsInstance;
	private Map<Product, String[]> productPrices;
	private int companyNum; // number of companies
	private String[] columnNames;
	private JFrame oFrame;
	private int blCol;
	private Object[][] displayData;
	
	public Command(JFrame frame) {
		oPluginFinder = new PluginFinder();
		oFrame = frame;
		blCol = -1; // doesn't exist
		displayData = null;
		productPrices = new HashMap<Product, String[]>();
	}
	
	/**
	 * Use the results returned from the pluginFinder, and
	 * attach the names to a checkbox and add the checkboxes
	 * to GUI.
	 */
	public void addExistingPlugins()
	{
		List<String> companies = oPluginFinder.find();
		for (String company : companies)
		{
			String className = "plugins." + company + "Analysis";
			try {
				Class<?> clazz = Class.forName(className);
				clsInstance = (CompetitorAnalysis) clazz.newInstance();
				if (clsInstance.isPluginCompleted() == true)
				{
					RetailGui.addCheckBox(company);	
				}
				
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			} catch (InstantiationException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
		}
	}
	
	public List<JCheckBox> getSelectedCheckBoxes()
	{
		JPanel rightPanel = RetailGui.getRightPanel();
		Component[] components = rightPanel.getComponents();
		List<JCheckBox> checkboxes = new ArrayList<JCheckBox>();
		
		for (Component c : components)
		{
			if (c instanceof JCheckBox)
			{
				System.out.println(c.getClass().getSimpleName());
				if (((JCheckBox) c).isSelected() == true)
				{
					checkboxes.add((JCheckBox) c);
				}
			}
		}
		
		return checkboxes;
	}
	
	public void compareCompanies()
	{
		List<JCheckBox> checkboxes = getSelectedCheckBoxes();
		int companyCount;
		
		if (checkboxes.size() < 2)
		{
			JOptionPane.showMessageDialog(oFrame, "Too few companies were selected.\n Please choose at least 2 companies to compare.", "Invalid Compare", JOptionPane.OK_OPTION);
		}
		else
		{
			productPrices = new HashMap<Product, String[]>();
			companyCount = 0;
			companyNum = checkboxes.size();
			columnNames = new String[checkboxes.size()+1];
			columnNames[0] = "Products";
			
			if (allowHiding(checkboxes) == true)
			{
				RetailGui.getHidingCheckbox().setVisible(true);
			}
			
			RetailGui.getHidingCheckbox().setEnabled(false);
			
			RetailGui.getFullProgressBar().setMaximum(companyNum);
			
			RetailGui.getFullProgressBar().setMinimum(0);
			
			Iterator<JCheckBox> i = checkboxes.iterator();
			
			while ((i.hasNext() == true) && (this.isCancelled() == false))
			{
				String companyName = i.next().getText();
				String className = "plugins." + companyName + "Analysis";
				System.out.println("company name: " + companyName);
				System.out.println("class name: " + className);
				
				try {
					Class<?> clazz = Class.forName(className);
					clsInstance = (CompetitorAnalysis) clazz.newInstance();
					clsInstance.setCommand(this);
					List<Product> products = clsInstance.getProducts();
					for (Product product : products)
					{
						addProductToList(product, companyCount, checkboxes.size());
					}
				} catch (ClassNotFoundException e) {
					e.printStackTrace();
				} catch (InstantiationException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				}
				companyCount++;
				columnNames[companyCount] = companyName;
				setStatusText(companyCount);
				
			}
			RetailGui.getItemFoundLabel().setText(productPrices.size() + " product item(s) found.");
			
			displayData = convertMapToArray(productPrices);
			displayToTable();
			
			Date now = new Date();
			Format format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			RetailGui.getDataDateLabel().setText("Data as of " + format.format(now));
			
			RetailGui.getHidingCheckbox().setEnabled(true);
			
			saveData(generateXMLFileName());
		}
	}
	
	public void cancel()
	{
		this.cancel(true);
		if (clsInstance != null)
		{
			clsInstance.cancel();
		}
		setStatusText(0);
		RetailGui.getCompanyStatus().setText("Process is cancelled...");
		RetailGui.getOverallStatus().setText("Process is cancelled...");
	}
	
	public void setCompanyLoadingStatus(String text)
	{
		RetailGui.getCompanyStatus().setText(text);
	}
	
	public void setOverallLoadingStatus(String text)
	{
		RetailGui.getOverallStatus().setText(text);
	}
	
	public void displayAllProducts()
	{
		displayData = convertMapToArray(productPrices);
		displayToTable();
	}
	
	public void hideNonBlProducts(TableRenderer myTableRenderer)
	{
		Map<Product, String[]> newProductList = new HashMap<Product, String[]>();
		Iterator<Entry<Product, String[]> > i = productPrices.entrySet().iterator();
		
		blCol = myTableRenderer.getBLColumn(RetailGui.getContentTable());
		
		if (blCol == -1)
		{
			System.out.println("No BL Column");
			return;
		}
		
		while (i.hasNext() == true)
		{
			Map.Entry<Product, String[]> pair = (Map.Entry<Product, String[]>) i.next();
			String[] prices = pair.getValue();
//			System.out.println("prices.length: " + prices.length);
//			System.out.println("blCol: " + blCol);
			if (prices[blCol - 1] != null)
			{
				newProductList.put(pair.getKey(), pair.getValue());
			}
			else
			{
				System.out.println(pair.getKey().getFullName() + ": " + pair.getValue());
			}
		}
		
		displayData = convertMapToArray(newProductList);
		displayToTable();
	}
	
	public void saveData(String fileName)
	{
		if (hasXMLExtension(fileName) == false)
		{
			fileName = addXMLExtension(fileName);
		}
		System.out.println(fileName);
		
		String xmlData = generateXML();
//		System.out.println(xmlData);
		PrintWriter writer = null;
		try {
			File file = new File(fileName);
			file.getParentFile().mkdirs();
			writer = new PrintWriter(file, "UTF-8");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		writer.print(xmlData);
		writer.close();
	}
	
	public void loadExistingData()
	{
		File dir = new File("Data");
		long lastModified = Long.MIN_VALUE;
		File dataFile = null;
		
		File[] matches = dir.listFiles(new FilenameFilter() {
			
			@Override
			public boolean accept(File dir, String name) {
				return name.startsWith("data") && name.endsWith(".xml");
			}
		});
		
		if (matches == null)
		{
			RetailGui.getDataDateLabel().setText("No recent data found.");
			return;
		}
		
		for (File f : matches)
		{
			if (f.lastModified() > lastModified)
			{
				dataFile = f;
				lastModified = f.lastModified();
			}
		}
		
		if (dataFile != null)
		{
			readXMLFile(dataFile);
			System.out.println("displaying to table");
			displayToTable();
			
			Date dataFileVersion = new Date(lastModified);
			Format format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			RetailGui.getDataDateLabel().setText("Data as of " + format.format(dataFileVersion));
		}
		else
		{
			RetailGui.getDataDateLabel().setText("No recent data found.");
		}
		
		
	}
	
	@Override
	protected Object doInBackground() throws Exception {
		{
			setStatusText(0);
			compareCompanies();
			return null;
		}
	}
	
	protected void done() {
		if (this.isCancelled() == false)
		{
			setStatusText(100);
	        RetailGui.getCompanyStatus().setText("All loaded!");
	        RetailGui.getOverallStatus().setText("All loaded!");
	        RetailGui.enableCompareButton();
	        RetailGui.disableCancelButton();
		}
    }
	
	private String generateXML()
	{
		String xmlData = addExcelXMLHeader();
		xmlData = addXMLTableHeaders(xmlData);
		
		for (int i = 0; i < displayData.length; ++i)
		{
			xmlData += "<ss:Row>" + "\n";
			for (int j = 0; j < displayData[0].length; ++j)
			{
				if (i == 0)
				{
					System.out.println(displayData[i][j]);
				}
				xmlData += createXMLCell(displayData[i][j].toString());
				
			}
			xmlData += "</ss:Row>" + "\n";
		}
		
		xmlData = addExcelXMLFooter(xmlData);
		
		return xmlData;
	}
	
	private String addExcelXMLHeader()
	{
		String header = "<?xml version=\"1.0\"?>" + "\n";
		header += "<ss:Workbook xmlns:ss=\"urn:schemas-microsoft-com:office:spreadsheet\">" + "\n";
//		header += "<ss:Styles>" + "\n";
//		header += "<ss:Style ss:ID=\"1\"" + "\n";
//		header += "ss:Font ss:Bold=\"1\"" + "\n";
//		header += "</ss:Style>" + "\n";
//		header += "</ss:Styles>" + "\n";
		
		header += "<ss:Worksheet ss:Name=\"Sheet1\">" + "\n";
		header += "<ss:Table>" + "\n";
		
		return header;
	}
	
	private String addExcelXMLFooter(String xmlData)
	{
		String footer = "</ss:Table>" + "\n";
		footer += "</ss:Worksheet>" + "\n";
		footer += "</ss:Workbook>" + "\n";
		
		return xmlData + footer;
	}
	
	private String createXMLCell(String value)
	{
		String cell = "<ss:Cell>" + "\n";
		cell += "<ss:Data ss:Type=\"String\">" + value + "</ss:Data>" + "\n";
		cell += "</ss:Cell>" + "\n";
		
		return cell;
	}
	
	private String generateXMLFileName()
	{
		String fileName = "Data/data";
		DateFormat dateFormat = new SimpleDateFormat("yyyy.MM.dd HH.mm.ss");
		Date date = new Date();
		fileName += " " + dateFormat.format(date);
		
		// add extension
		fileName += ".xml";
		
		return fileName;
	}
	
	private boolean hasXMLExtension(String fileName)
	{
		boolean hasExtension = true;
		if (fileName.indexOf(".xml") == -1)
		{
			hasExtension = false;
		}
		return hasExtension;
	}
	
	private String addXMLExtension(String fileName)
	{
		return fileName += ".xml";
	}
	
	private void readXMLFile(File file)
	{
		int cols = 0, rows = 0;
		String content = ""; // save content as csv
		
		try {
			BufferedReader br = new BufferedReader(new FileReader(file));
			String line;
			int i = -1, j = -1;
			
			while ((line = br.readLine()) != null)
			{
				if (line.contains("<ss:Row>") == true)
				{
					i++;
					rows++;
					j = -1;
					if (content.isEmpty() == false)
					{
						content += "\n";
					}
					
					cols = 0;
				}
				if (line.contains("<ss:Cell>") == true)
				{
					j++;
					cols++;
					while(line.contains("<ss:Data") == false)
					{
						line = br.readLine();
					}
					String data = extractContent(line, ">(.*?)<");
					
					if (j != 0)
					{
						content += ",";
					}
					content += "\"" + data + "\"";
				}
			}
			br.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		displayData = new Object[rows - 1][cols];
		
		Scanner scanner = new Scanner(content);
		String line;
		String[] fields = new String[cols];
		
		line = scanner.nextLine();
		fields = line.split("\",\"");
		columnNames = new String[cols];
		companyNum = fields.length - 1;
		for (int j = 0; j < fields.length; ++j)
		{
			fields[j] = fields[j].replace("\"", "");
			columnNames[j] = fields[j];
		}
		
		while(scanner.hasNext())
		{
			for (int i = 0; i < rows - 1; ++i)
			{
				if (scanner.hasNextLine() == true)
				{
					line = scanner.nextLine();
					fields = line.split("\",\"");
					String[] prices = new String[fields.length - 1];
					Product product = null;
					for (int j = 0; j < fields.length; ++j)
					{
						fields[j] = fields[j].replace("\"", "");
						displayData[i][j] = fields[j];
						if (j == 0)
						{
							product = new Product(fields[j], "0");
						}
						else
						{
							if (fields[j].equals("-"))
							{
								prices[j - 1] = null;
							}
							else
							{
								prices[j - 1] = fields[j];
							}
						}
					}
					productPrices.put(product, prices);
				}

			}
		}
		scanner.close();
		
		RetailGui.getItemFoundLabel().setText(rows + " product item(s) found.");
	}
	
	private boolean allowHiding(List<JCheckBox> checkboxes)
	{
		Iterator<JCheckBox> i = checkboxes.iterator();
		boolean hasBL = false;
		
		while (i.hasNext() == true)
		{
			if (i.next().getText().equals("BullionList") == true)
			{
				hasBL = true;
			}
		}
		
		return hasBL;
	}
	
	private String addXMLTableHeaders(String xmlData)
	{
		String header = "<ss:Row>" + "\n";
		
		for (int i = 0; i < columnNames.length; ++i)
		{
			header += createXMLCell(columnNames[i]);
		}
		
		header += "</ss:Row>" + "\n";
		
		return xmlData + header;
	}
	
	private void displayToTable()
	{
		
//		displayData = convertMapToArray(productPrices);
		RetailGui.getContentTable().setModel(new DefaultTableModel(displayData, columnNames) {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
		    public boolean isCellEditable(int row, int column) {
		        return false;
		    }
			
			@Override
	         public Class<?> getColumnClass(int columnIndex) {
	            return String.class;
	         }
		});
		
		sortTable();
		
		((DefaultTableModel) (RetailGui.getContentTable().getModel())).fireTableDataChanged();
		RetailGui.getContentTable().repaint();
	}

	private void sortTable()
	{
		TableRowSorter<TableModel> sorter = new TableRowSorter<>(RetailGui.getContentTable().getModel());
		RetailGui.getContentTable().setRowSorter(sorter);
		RetailGui.getContentTable().setDefaultRenderer(Object.class, new TableRenderer());
		List<RowSorter.SortKey> sortKeys = new ArrayList<>();
		
		int columnIndexToSort = 0;
		sortKeys.add(new RowSorter.SortKey(columnIndexToSort, SortOrder.ASCENDING));
		
		sorter.setSortKeys(sortKeys);
		sorter.sort();
		
		RetailGui.getContentTable().repaint();
	}
	
	private void addProductToList(Product product, int companyCount, int companyNum)
	{
		if (productPrices.containsKey(product) == true) // has product in list
		{
//			System.out.println("Product: " + product.getFullName() + " is already in the list.");
			String[] prices = productPrices.get(product);
			prices[companyCount] = product.getPrice();
		}
		else // has no product in list
		{
			String[] prices = new String[companyNum];
			prices[companyCount] = product.getPrice();
			productPrices.put(product, prices);
		}
	}
	
	private Object[][] convertMapToArray(Map<Product, String[]> productList)
	{
		Object[][] data = new Object[productList.size()][companyNum+1];
//		System.out.println(data.length + " " + data[0].length);
		int i = 0;
		Iterator<Entry<Product, String[]>> it = productList.entrySet().iterator();
		while (it.hasNext())
		{
			Map.Entry<Product, String[]> pairs = it.next();
			data[i][0] = pairs.getKey().getFullName();
			System.out.println(data[i][0]);
			int j = 1;
			for (String p : pairs.getValue())
			{
//				System.out.print(p + ", ");
				if (p == null)
				{
					p = "-";
				}
				data[i][j] = p;
				j++;
			}
			
			i++;
//			System.out.println();
		}
		
		return data;
	}
	
	private void setStatusText(final int status) {
        SwingUtilities.invokeLater(new Runnable(){
 
            //@Override
            public void run() {
 
                RetailGui.getFullProgressBar().setValue(status);
            }
 
        });
    }
	
	private String extractContent(String line, String regex)
	{
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(line);
		String price = "";
		
		while (matcher.find() == true)
		{
			price = matcher.group(1);
		}
		
		return price;
	}
	
//	private void printMap()
//	{
//		Iterator<Entry<Product, String[]>> it = productPrices.entrySet().iterator();
//		while (it.hasNext())
//		{
//			Map.Entry<Product, String[]> pairs = it.next();
//			System.out.print(pairs.getKey().getFullName() + ": ");
//			for (String p : pairs.getValue())
//			{
//				System.out.print(p + ", ");
//			}
//			System.out.println();
//		}
//	}


}
