package misc;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class PluginFinder {
	private String oDirectoryPath = PluginFinder.class.getProtectionDomain().getCodeSource().getLocation().getPath();
	private List<String> oPlugins;
	
	public PluginFinder()
	{
		oPlugins = new ArrayList<String>();
		try {
			oDirectoryPath = URLDecoder.decode(oDirectoryPath, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	// find available plugin classes in /bin file
	public List<String> find()
	{
		if (isJarFile())
		{
			findPluginsJAR();
			return oPlugins;
		}
		else
		{
			findPlugins();
			return oPlugins;
		}
	}
	
	// for finding plugins when run in Eclipse
	private void findPlugins()
	{
		File dir = new File(oDirectoryPath + "/plugins/");
//		System.out.println("oDirectoryPath: " + dir.getAbsolutePath());
		File[] matches = dir.listFiles();
		String fileName = "";
		
		if (matches == null)
		{
			return;
		}
		
		for (File file : matches)
		{
			if (file.getName().contains("Analysis") == true&&
				file.getName().contains("CompetitorAnalysis") == false)
			{
				fileName = getCompanyName(file.getName());
				System.out.println(fileName);
				if (oPlugins.contains(fileName) == false)
				{
					oPlugins.add(fileName);
				}
			}
		}
	}
	
	// find plugins specific for use in executable JAR file
	private void findPluginsJAR()
	{
//		List<String> classNames = new ArrayList<String>();
//		System.out.println("inside findPluginsJAR");
		ZipInputStream zip;
		try {
//			System.out.println("oDirectoryPath: " + oDirectoryPath);
//			File f = new File(URLDecoder.decode(oDirectoryPath, "UTF-8"));
			zip = new ZipInputStream(new FileInputStream(oDirectoryPath));
			
			for (ZipEntry entry = zip.getNextEntry(); entry != null; entry = zip.getNextEntry())
			{
				if (!entry.isDirectory() && entry.getName().endsWith(".class")
					&& entry.getName().contains("Analysis") == true && entry.getName().contains("CompetitorAnalysis") == false)
				{
					String fileName = entry.getName();
					fileName = fileName.replaceAll("plugins/", "");
					fileName = fileName.replaceAll(".class", "");
					fileName = fileName.replace("Analysis", "");
//					System.out.println(fileName);
					oPlugins.add(fileName);
				}
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	// extract the company name from the file name.
	private String getCompanyName(String fileName)
	{
		int pos = fileName.indexOf("Analysis");
		return fileName.substring(0, pos);
	}
	
	// for determining if the path found is an executable JAR file.
	// need a separate implementation for searching if it's a JAR file.
	private boolean isJarFile()
	{
		if (oDirectoryPath.endsWith(".jar"))
		{
			System.out.println("it is a JAR file");
			return true;
		}
		
		System.out.println("it is NOT a JAR file");
		return false;
	}
}
