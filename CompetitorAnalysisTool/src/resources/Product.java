package resources;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Product {
	private String oFullName;
	private String oSize;
	private String oMetalType;
	private String oYear;
	private String oName;
	private String oPrice;
	
	public Product(String name, String price)
	{
		this.oFullName = name;
		this.oPrice = price;
		this.oYear = extractDetails(oFullName, "\\s*\\d{4}\\s*");
		this.oMetalType = extractDetails(oFullName, "Silver|Gold|Platinum|Palladium|Copper");
		this.oSize = extractDetails(oFullName, "\\d+[\\S]*\\s{0,3}[oz|kg|gm|g]+");
		extractName();
		oYear = removeExcessiveSpace(oYear);
		oMetalType = removeExcessiveSpace(oMetalType);
		oSize = oSize.replaceAll("\\s*", "");
//		System.out.println(oName + ", " + oYear + ", " + oMetalType + ", " + oSize);
		
		reconstructFullName();
		formatPrice();
//		System.out.println(oFullName);
	}
	
	public String getName()
	{
		return oName;
	}

	public void setName(String name)
	{
		this.oName = name;
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((oMetalType == null) ? 0 : oMetalType.hashCode());
//		result = prime * result + ((oName == null) ? 0 : oName.hashCode());
		result = prime * result + ((oSize == null) ? 0 : oSize.hashCode());
//		result = prime * result + ((oYear == null) ? 0 : oYear.hashCode());
//		System.out.println("product hashcode: " + result);
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
		{
			return true;
		}
		if (obj == null)
		{
			return false;
		}
		if (getClass() != obj.getClass())
		{
			return false;
		}
		
		Product other = (Product) obj;
//		System.out.println("this.name: " + oName);
//		System.out.println("other.name: " + other.getName());
//		
//		System.out.println("checking metals");
//		System.out.println("oMetalType: " + oMetalType);
//		System.out.println("other.metal: " + other.getMetalType());
		if (oMetalType == null)
		{
			if (other.oMetalType != null)
			{
				return false;
			}
		} else
		{
			if (oMetalType.equals(other.oMetalType) == false)
			{
				return false;
			}
		}
		
//		System.out.println("Checking names");
			
		if (oName == null)
		{
			if (other.oName != null)
			{
				return false;
			}
		}
		else
		{
//			System.out.println("oName is not null");
			if (similarNames(oName, other.oName) == false)
			{
				return false;
			}
		}
			
		if (oSize == null)
		{
			if (other.oSize != null)
			{
				return false;
			}
		}
		else
		{
			if (oSize.equals(other.oSize) == false)
			{
				return false;
			}
		}
			
		if (oYear == null)
		{
//			System.out.println("oYear: " + oYear);
//			System.out.println("other.oYear: " + other.oYear);
//			if (other.oYear != null)
//			{
//				return false;
//			}
		}
		else
		{
//			System.out.println("oYear: " + oYear);
//			System.out.println("other.oYear: " + other.oYear);
			if (other.oYear == null)
			{
				return true;
			}
			
//			if (oYear.equals(other.oYear) == false)
//			{
//				return false;
//			}
		}
			
		return true;
	}

	public String getSize()
	{
		return oSize;
	}

	public void setSize(String Size)
	{
		this.oSize = Size;
	}

	public String getMetalType() {
		return oMetalType;
	}

	public void setMetalType(String metalType)
	{
		this.oMetalType = metalType;
	}

	public String getYear()
	{
		return oYear;
	}

	public void setYear(String year)
	{
		this.oYear = year;
	}

	public String getFullName()
	{
		return oFullName;
	}

	public void setFullName(String name)
	{
		this.oFullName = name;
	}

	public String getPrice()
	{
		return oPrice;
	}

	public void setPrice(String price)
	{
		this.oPrice = price;
	}
	
	private String extractDetails(String name, String regex)
	{
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(name);
		String result = "";
		
		while (matcher.find() == true)
		{
			result = matcher.group();
		}
		
		return result;
	}
	
	private void extractName()
	{
		oName = oFullName;
//		System.out.println(oFullName);
		if (oYear.isEmpty() == false)
		{
			oName = oName.replace(oYear, " ");
		}
		if (oMetalType.isEmpty() == false)
		{
			oName = oName.replace(oMetalType, " ");
		}
		if (oSize.isEmpty() == false)
		{
			oName = oName.replace(oSize, " ");
		}
//		System.out.println(oName);
		
//		System.out.println(oName);
		
		oName = oName.replaceAll("\"|\'|\\(|\\)|-", "");
		oName = removeExcessiveSpace(oName);
	}
	
	private String removeExcessiveSpace(String prop)
	{
		prop = prop.replaceAll("\\s+", " ");
		prop = prop.replaceAll("^\\s+|\\s+$", "");
		return prop;
	}
	
	private void reconstructFullName()
	{
		oFullName = "";
		if (oSize.isEmpty() == false)
		{
			oFullName += oSize + " ";
		}
		if (oSize.isEmpty() == false)
		{
			oFullName += oMetalType + " ";
		}
		if (oYear.isEmpty() == false)
		{
			oFullName += oYear + " ";
		}
		oFullName += oName;
	}
	
	private void formatPrice()
	{
		if (this.oPrice.contains("$") == false)
		{
			setPrice("$" + this.oPrice);
		}
	}
	
	private boolean similarNames(String name1, String name2)
	{
		boolean isSimilar = true;
		
//		System.out.println("inside similarNames");
		
//		System.out.println("name1: " + name1 + " name2: " + name2);
		if (name1.contains("Trade-In") == true || name2.contains("Trade-In") == true)
		{
			isSimilar = false;
		}
		
		if (isSimilar == true && name1.length() == name2.length())
		{
			String[] tokens = name1.split("\\s+");
			for (String token : tokens)
			{
//				System.out.println(token);
				if (name2.contains(token) == false)
				{
					isSimilar = false;
				}
			}
		}
		else
		{
			if (name1.contains("PAMP") == true && name1.contains("Fortuna") == true &&
				name2.contains("PAMP") == true && name2.contains("Minted") == true)
			{
				isSimilar = true;
			}
			else
			{
				if (isSimilar == true)
				{
					String[] tokens = null;
					String longerName = "";
//					System.out.println("name1: " + name1);
//					System.out.println("name2: " + name2);
					if (name1.length() < name2.length())
					{
						tokens = name1.split("\\s+");
						longerName = name2;
					}
					else
					{
						tokens = name2.split("\\s+");
						longerName = name1;
					}
//					System.out.println("longer name: " + longerName);
					for (String token : tokens)
					{
//						System.out.println("token: " + token);
						if ((token.equalsIgnoreCase("minted") == true ||
								token.equalsIgnoreCase("cast") == true) &&
								name2.contains(token) == false)
						{
							
							isSimilar = true;
						}
						else if (longerName.contains(token) == false)
						{
//							System.out.println("longer name doesn't contain: " + token);
							isSimilar = false;
						}
					}
				}
			}
			
		}
		
//		System.out.println("isSimilar: " + isSimilar);
		return isSimilar;
	}
}
