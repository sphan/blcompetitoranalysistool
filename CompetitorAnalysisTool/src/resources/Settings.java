package resources;

public class Settings {
	
	private boolean oAutomationEnabled;
	private boolean oRetrievalOnStartUp;
	
	public Settings()
	{
		oAutomationEnabled = false;
		oRetrievalOnStartUp = false;
	}

	public boolean isoAutomationEnabled()
	{
		return oAutomationEnabled;
	}

	public boolean isoRetrievalOnStartUp()
	{
		return oRetrievalOnStartUp;
	}

	public void setoAutomationEnabled(boolean oAutomationEnabled)
	{
		this.oAutomationEnabled = oAutomationEnabled;
	}

	public void setoRetrievalOnStartUp(boolean oRetrievalOnStartUp)
	{
		this.oRetrievalOnStartUp = oRetrievalOnStartUp;
	}
}
