package gui;

import java.awt.Color;
import java.awt.Image;

import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JLabel;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.JCheckBox;
import javax.swing.JRadioButton;
import javax.swing.JToolTip;
import javax.swing.border.BevelBorder;
import javax.swing.border.SoftBevelBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.JToggleButton;

import resources.Settings;

public class SettingsGui {
	
	private JFrame oFrame;
	private MainMenuGui oMainMenu;
	private Settings oSettings;
	
	private JButton oBtnBack;
	private JLabel lblSettings;
	private JPanel oAutomateRetrievalPanel;
	private JCheckBox oChckbxEnableAutomaticRetrieval;
	private JRadioButton oRdbtnEvery30;
	private JRadioButton oRdbtnEvery60;
	private JRadioButton oRdbtnEvery90;
	private JRadioButton oRdbtnEvery120;
	private ButtonGroup automatedIntervals;
	private JPanel oRetrieveOnStartPanel;
	private JCheckBox chckbxEnableRetrievalOn;
	private JButton oBtnSaveSettings;
	private JButton oBtnCancel;
	
	public SettingsGui()
	{
		oMainMenu = new MainMenuGui();
		oSettings = new Settings();
		initialize();
	}
	
	public JFrame getFrame()
	{
		return oFrame;
	}
	
	public void initialize()
	{
		oFrame = new JFrame();
		oFrame.getContentPane().setBackground(new Color(240, 248, 255));
		oFrame.setTitle("Competitor Analysis - Settings");
		
		oBtnBack = new JButton("Back To Main Menu")
		{
			@Override
			public JToolTip createToolTip()
			{
				return (new MyToolTip(this));
			}
		};
		oBtnBack.setToolTipText("Back to Main Menu");
		oBtnBack.setBounds(10, 11, 40, 40);
		oBtnBack.setOpaque(false);
		oBtnBack.setContentAreaFilled(false);
		oBtnBack.setBorderPainted(false);
		try {
			Image backImg = ImageIO.read(getClass().getResource("/resources/back-arrow.png"));
			Image backIcon = backImg.getScaledInstance(oBtnBack.getWidth(), oBtnBack.getHeight(), Image.SCALE_SMOOTH);
			oBtnBack.setIcon(new ImageIcon(backIcon));
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		oBtnBack.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				oMainMenu.getFrame().setVisible(true);
				oFrame.setVisible(false);
			}
		});
		oFrame.getContentPane().setLayout(null);
		oFrame.getContentPane().add(oBtnBack);
		
		lblSettings = new JLabel("Settings");
		lblSettings.setBounds(218, 11, 98, 29);
		lblSettings.setForeground(new Color(128, 0, 0));
		lblSettings.setFont(new Font("Tahoma", Font.BOLD, 24));
		oFrame.getContentPane().add(lblSettings);
		
		oAutomateRetrievalPanel = new JPanel();
		oAutomateRetrievalPanel.setBorder(new SoftBevelBorder(BevelBorder.RAISED, null, null, null, null));
		oAutomateRetrievalPanel.setBounds(67, 82, 404, 82);
		oAutomateRetrievalPanel.setBackground(new Color(240,248,255));
		oFrame.getContentPane().add(oAutomateRetrievalPanel);
		oAutomateRetrievalPanel.setLayout(null);
		
		oChckbxEnableAutomaticRetrieval = new JCheckBox("Enable Automatic Retrieval")
		{
			@Override
			public JToolTip createToolTip()
			{
				return (new MyToolTip(this));
			}
		};
		oChckbxEnableAutomaticRetrieval.setToolTipText("<html><p width=\\\"100\\\">This is by default disabled.<br />\r\nEnable it and choose an interval to allow <br /> automatic data retrieval from all available sites <br /> for every interval that you have chosen.</p></html>");
		oChckbxEnableAutomaticRetrieval.setBounds(21, 7, 342, 23);
		oChckbxEnableAutomaticRetrieval.setBackground(new Color(240,248,255));
		oChckbxEnableAutomaticRetrieval.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if (oChckbxEnableAutomaticRetrieval.isSelected() == true)
				{
					enableIntervalRadios();
				}
				else
				{
					disableIntervalRadios();
				}
			}
		});
		oAutomateRetrievalPanel.add(oChckbxEnableAutomaticRetrieval);
		
		oRdbtnEvery30 = new JRadioButton("Every 30 minutes");
		oRdbtnEvery30.setBounds(56, 33, 136, 23);
		oRdbtnEvery30.setBackground(new Color(240,248,255));
		oAutomateRetrievalPanel.add(oRdbtnEvery30);
		
		oRdbtnEvery60 = new JRadioButton("Every 60 minutes");
		oRdbtnEvery60.setBounds(56, 52, 136, 23);
		oRdbtnEvery60.setBackground(new Color(240,248,255));
		oAutomateRetrievalPanel.add(oRdbtnEvery60);
		
		oRdbtnEvery90 = new JRadioButton("Every 90 minutes");
		oRdbtnEvery90.setBounds(227, 33, 136, 23);
		oRdbtnEvery90.setBackground(new Color(240,248,255));
		oAutomateRetrievalPanel.add(oRdbtnEvery90);
		
		oRdbtnEvery120 = new JRadioButton("Every 120 minutes");
		oRdbtnEvery120.setBounds(227, 52, 136, 23);
		oRdbtnEvery120.setBackground(new Color(240,248,255));
		
		automatedIntervals = new ButtonGroup();
		automatedIntervals.add(oRdbtnEvery30);
		automatedIntervals.add(oRdbtnEvery60);
		automatedIntervals.add(oRdbtnEvery90);
		automatedIntervals.add(oRdbtnEvery120);
		
		if (oSettings.isoAutomationEnabled() == true)
		{
			oChckbxEnableAutomaticRetrieval.setSelected(true);
			enableIntervalRadios();
		}
		else
		{
			oChckbxEnableAutomaticRetrieval.setSelected(false);
			disableIntervalRadios();
		}
		
		oAutomateRetrievalPanel.add(oRdbtnEvery120);
		
		oRetrieveOnStartPanel = new JPanel();
		oRetrieveOnStartPanel.setLayout(null);
		oRetrieveOnStartPanel.setBorder(new SoftBevelBorder(BevelBorder.RAISED, null, null, null, null));
		oRetrieveOnStartPanel.setBackground(new Color(240, 248, 255));
		oRetrieveOnStartPanel.setBounds(67, 195, 404, 40);
		oFrame.getContentPane().add(oRetrieveOnStartPanel);
		
		chckbxEnableRetrievalOn = new JCheckBox("Enable Retrieval on Start Up")
		{
			@Override
			public JToolTip createToolTip()
			{
				return (new MyToolTip(this));
			}
		};
		chckbxEnableRetrievalOn.setToolTipText("<html><p width=\\\"100\\\">This is by default disabled.<br />\r\nEnable it to allow automatic data retrieval on start up.</p></html>");
		chckbxEnableRetrievalOn.setBackground(new Color(240, 248, 255));
		chckbxEnableRetrievalOn.setBounds(20, 7, 342, 23);
		if (oSettings.isoRetrievalOnStartUp() == true)
		{
			chckbxEnableRetrievalOn.setSelected(true);
		}
		else
		{
			chckbxEnableRetrievalOn.setSelected(false);	
		}
		oRetrieveOnStartPanel.add(chckbxEnableRetrievalOn);
		
		oBtnSaveSettings = new JButton("Save");
		oBtnSaveSettings.setBackground(new Color(144, 238, 144));
		oBtnSaveSettings.setBounds(335, 360, 136, 49);
		oBtnSaveSettings.setFont(new Font("Tahoma", Font.BOLD, 14));
		oBtnSaveSettings.setBorder(new SoftBevelBorder(BevelBorder.RAISED, null, null, null, null));
		oFrame.getContentPane().add(oBtnSaveSettings);
		
		oBtnCancel = new JButton("Cancel");
		oBtnCancel.setBackground(new Color(255, 99, 71));
		oBtnCancel.setBounds(157, 360, 136, 49);
		oBtnCancel.setFont(new Font("Tahoma", Font.BOLD, 14));
		oBtnCancel.setBorder(new SoftBevelBorder(BevelBorder.RAISED, null, null, null, null));
		oBtnCancel.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				oMainMenu.getFrame().setVisible(true);
				oFrame.setVisible(false);
			}
		});
		oFrame.getContentPane().add(oBtnCancel);
		
		oFrame.setBounds(100, 100, 590, 572);
	}
	
	private void enableIntervalRadios()
	{
		oRdbtnEvery30.setEnabled(true);
		oRdbtnEvery60.setEnabled(true);
		oRdbtnEvery90.setEnabled(true);
		oRdbtnEvery120.setEnabled(true);
	}
	
	private void disableIntervalRadios()
	{
		oRdbtnEvery30.setEnabled(false);
		oRdbtnEvery60.setEnabled(false);
		oRdbtnEvery90.setEnabled(false);
		oRdbtnEvery120.setEnabled(false);
	}
}
