package gui;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JToolTip;

public class MainMenuGui {

	private JFrame oFrame;
	private JButton oBtnCompareRetail;
	private JButton oBtnCompareWhole;
	private JButton oBtnEnterSimilarProducts;
	private JButton oBtnHelp;
	private JButton oBtnGetBlProduct;
	private JButton oBtnGetIndividualProduct;
	private JButton oBtnSettings;
	
	/**
	 * Launch the application.
	 * @wbp.parser.entryPoint
	 */
	public static void main(String[] args)
	{
		EventQueue.invokeLater(new Runnable()
		{
			public void run()
			{
				try
				{
					MainMenuGui window = new MainMenuGui();
					window.oFrame.setVisible(true);
				}
				catch (Exception e){
					e.printStackTrace();
				}
			}
		});
	}
	
	public MainMenuGui()
	{
		initialize();
	}
	
	public JFrame getFrame()
	{
		return oFrame;
	}
	
	private void initialize()
	{
		oFrame = new JFrame();
		oFrame.getContentPane().setBackground(new Color(240, 248, 255));
		oFrame.setBounds(100, 100, 590, 572);
		oFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		oFrame.getContentPane().setLayout(null);
		oFrame.setTitle("Competitor Analysis");
		
		oBtnCompareRetail = new JButton("Compare Retail Prices")
		{
			@Override
			public JToolTip createToolTip()
			{
				return (new MyToolTip(this));
			}
		};
		oBtnCompareRetail.setToolTipText("Compare Retail Prices Against Competitors");
		oBtnCompareRetail.setBounds(73, 214, 181, 92);
//		oBtnCompareRetail.setBackground(new Color(102,153,153));
		oBtnCompareRetail.setForeground(new Color(0,51,51));
		oBtnCompareRetail.setFont(new Font("Tahoma", Font.BOLD, 14));
		oBtnCompareRetail.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				try
				{
					RetailGui window = new RetailGui();
					window.getFrame().setVisible(true);
					oFrame.setVisible(false);
				}
				catch (Exception e1){
					e1.printStackTrace();
				}
			}
		});
		oFrame.getContentPane().add(oBtnCompareRetail);
		
		oBtnCompareWhole = new JButton("Compare Wholesale Prices")
		{
			@Override
			public JToolTip createToolTip()
			{
				return (new MyToolTip(this));
			}
		};
		oBtnCompareWhole.setToolTipText("Compare Wholesale Price Against Competitors");
		oBtnCompareWhole.setBounds(312, 214, 181, 92);
//		oBtnCompareWhole.setBackground(new Color(102,153,153));
		oBtnCompareWhole.setForeground(new Color(0,51,51));
		oBtnCompareWhole.setFont(new Font("Tahoma", Font.BOLD, 14));
		oFrame.getContentPane().add(oBtnCompareWhole);
		
		oBtnEnterSimilarProducts = new JButton("Enter Similar Products")
		{
			@Override
			public JToolTip createToolTip()
			{
				return (new MyToolTip(this));
			}
		};
		oBtnEnterSimilarProducts.setToolTipText("Enter Products with Similar Names");
		oBtnEnterSimilarProducts.setBounds(73, 343, 181, 92);
//		oBtnEnterSimilarProducts.setBackground(new Color(102,153,153));
		oBtnEnterSimilarProducts.setForeground(new Color(0,51,51));
		oBtnEnterSimilarProducts.setFont(new Font("Tahoma", Font.BOLD, 14));
		oFrame.getContentPane().add(oBtnEnterSimilarProducts);
		
		oBtnHelp = new JButton("Help")
		{
			@Override
			public JToolTip createToolTip()
			{
				return (new MyToolTip(this));
			}
		};
		oBtnHelp.setToolTipText("Help Menu");
		oBtnHelp.setBounds(312, 343, 181, 92);
//		oBtnHelp.setBackground(new Color(102,153,153));
		oBtnHelp.setForeground(new Color(0,51,51));
		oBtnHelp.setFont(new Font("Tahoma", Font.BOLD, 14));
		oFrame.getContentPane().add(oBtnHelp);
		
		oBtnGetBlProduct = new JButton("Get BL Product List")
		{
			@Override
			public JToolTip createToolTip()
			{
				return (new MyToolTip(this));
			}
		};
		oBtnGetBlProduct.setToolTipText("Get Bullion Product List");
		oBtnGetBlProduct.setBounds(73, 82, 181, 92);
//		oBtnGetBlProduct.setBackground(new Color(102,153,153));
		oBtnGetBlProduct.setForeground(new Color(0,51,51));
		oBtnGetBlProduct.setFont(new Font("Tahoma", Font.BOLD, 14));
		oFrame.getContentPane().add(oBtnGetBlProduct);
		
		oBtnGetIndividualProduct = new JButton("Get Individual Product Prices")
		{
			@Override
			public JToolTip createToolTip()
			{
				return (new MyToolTip(this));
			}
		};
		oBtnGetIndividualProduct.setToolTipText("Get Individual Product Prices");
		oBtnGetIndividualProduct.setBounds(312, 82, 181, 92);
//		oBtnGetIndividualProduct.setBackground(new Color(102,153,153));
		oBtnGetIndividualProduct.setForeground(new Color(0,51,51));
		oBtnGetIndividualProduct.setFont(new Font("Tahoma", Font.BOLD, 14));
		oFrame.getContentPane().add(oBtnGetIndividualProduct);
		
		oBtnSettings = new JButton("Settings")
		{
			@Override
			public JToolTip createToolTip()
			{
				return (new MyToolTip(this));
			}
		};
		oBtnSettings.setToolTipText("Settings");
		oBtnSettings.setBounds(10, 11, 40, 40);
		oBtnSettings.setOpaque(false);
		oBtnSettings.setContentAreaFilled(false);
		oBtnSettings.setBorderPainted(false);
		try {
			Image settingImg = ImageIO.read(getClass().getResource("/resources/settings.png"));
			Image settingIcon = settingImg.getScaledInstance(oBtnSettings.getWidth(), oBtnSettings.getHeight(), Image.SCALE_SMOOTH);
			oBtnSettings.setIcon(new ImageIcon(settingIcon));
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		oBtnSettings.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				SettingsGui setting = new SettingsGui();
				setting.getFrame().setVisible(true);
				oFrame.setVisible(false);
			}
		});
		
		oFrame.getContentPane().add(oBtnSettings);
	}
}
