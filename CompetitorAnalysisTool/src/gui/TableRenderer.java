package gui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.util.Enumeration;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;

public class TableRenderer extends DefaultTableCellRenderer
{
	private int blCol; // the column bl is in (try to detect the row)
	private Color redFg;
	private Color greenFg;
	private Color originalFg;
	private Color redDarker;
	private Color greenDarker;
	private Color originalDarker;
	
	public TableRenderer()
	{
		super();
		blCol = -1;
		greenFg = new Color(132, 224, 156);
		originalFg = new Color(237, 237, 237);
		redFg = Color.RED;
		redDarker = new Color(224, 0, 0);
		greenDarker = new Color(0, 171, 90);
		originalDarker = new Color(46, 46, 46);
	}

	@Override
	public Component getTableCellRendererComponent(JTable table, Object value,
			boolean isSelected, boolean hasFocus, int row, int column) {
		float blPrice = 0;
		float otherPrice = 0;
		
		if (value == null)
			return this;
		
//		row = table.convertRowIndexToModel(row);
//		column = table.convertColumnIndexToModel(column);
		
		Component renderer = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
		
		
		
		blCol = getBLColumn(table);
		// System.out.println("blCol: " + blCol);
		if (blCol == -1)
		{
			return this;
		}
		
		if (column != 0 && blCol != -1)
		{
			blPrice = convertToFloat((String) table.getModel().getValueAt(table.convertRowIndexToModel(row), blCol));
			otherPrice = convertToFloat((String) table.getModel().getValueAt(table.convertRowIndexToModel(row), column));
			
			if (blPrice != 0 && otherPrice != 0)
			{
				if (blPrice > otherPrice)
				{
//					System.out.println("Setting foreground to red");
					renderer.setForeground(redFg);
				}
				else
				{
					if (blPrice < otherPrice)
					{
//						System.out.println("Setting foreground to green");
						renderer.setForeground(greenFg);
					}
					else
					{
//						System.out.println("Setting foreground to black");
						renderer.setForeground(originalFg);
					}
				}
			}
		}
		else
		{
			renderer.setForeground(originalFg);
		}
		
		if (table.isCellSelected(row, column) == true)
		{
			renderer.setFont(renderer.getFont().deriveFont(Font.BOLD));
			if (renderer.getForeground().equals(greenFg))
			{
				renderer.setForeground(greenDarker);
			}
			else if (renderer.getForeground().equals(redFg))
			{
				renderer.setForeground(redDarker);
			}
			else if (renderer.getForeground().equals(originalFg))
			{
				renderer.setForeground(originalDarker);
			}
		}
		
		return this;
	}
	
	public int getBLColumn(JTable table)
	{
//		String header = "";
		JTableHeader th = table.getTableHeader();
		Enumeration<TableColumn> columns = th.getColumnModel().getColumns();
		
		while (columns.hasMoreElements())
		{
			TableColumn tc = columns.nextElement();
			
			if (tc.getHeaderValue().toString().equals("BullionList"))
			{
				return tc.getModelIndex();
			}
		}
		return -1;
	}

	private float convertToFloat(String aPrice)
	{
		float price = 0;
		
		aPrice = aPrice.replace("$", "");
		aPrice = aPrice.replaceAll("$|-|,", "");
		
		if (aPrice.isEmpty() == true)
		{
			return price;
		}
		
		if (aPrice.isEmpty() == false || aPrice != null)
		{
			try
			{
				price = Float.parseFloat(aPrice);
			}
			catch (NumberFormatException e)
			{
				e.printStackTrace();
			}
		}
		
		return price;
	}
}
