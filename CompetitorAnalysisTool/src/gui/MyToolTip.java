package gui;

import java.awt.Color;

import javax.swing.JComponent;
import javax.swing.JToolTip;

@SuppressWarnings("serial")
public class MyToolTip extends JToolTip
{

	public MyToolTip(JComponent component)
	{
		super();
		setComponent(component);
		setBackground(new Color(255,255,209));
	}
}
