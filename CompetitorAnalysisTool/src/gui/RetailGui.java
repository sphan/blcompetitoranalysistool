package gui;

import javax.imageio.ImageIO;
import javax.swing.JFrame;

import java.awt.BorderLayout;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.SwingUtilities;

import misc.Command;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.Color;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;
import java.io.IOException;

import javax.swing.BoxLayout;
import javax.swing.JScrollPane;

public class RetailGui {

	private JFrame oFrame;
	private static JTable oContentTable;
	
	private JPanel oTopPanel;
	private static JProgressBar oCompanyProgress;
	private static JProgressBar oFullProgress;
	private static JCheckBox chckbxHideNonblProducts;
	
	private static JPanel oRightPanel;
	private static JLabel overallStatus;
	private static JLabel companyStatus;
	
	private static JButton oCompareButton;
	private static JButton oCancelButton;
	private static JButton oSaveToFileButton;
	
	private static JLabel itemFoundLbl;
	
	private Command command;
	
	private final TableRenderer myTableRenderer = new TableRenderer();
	private static JLabel dataDateLbl;
	private JButton btnBackToMainMenu;

	/**
	 * Create the application.
	 */
	public RetailGui()
	{
		initialize();
		command = new Command(oFrame);
		command.addExistingPlugins();
		addCompareButton();
		addCancelButton();
		addSaveToFileButton();
		command.loadExistingData();
		
		if (myTableRenderer.getBLColumn(oContentTable) != -1)
		{
			chckbxHideNonblProducts.setVisible(true);
		}
	}
	
	public JFrame getFrame()
	{
		return oFrame;
	}
	
	
	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize()
	{
		JLabel instructionLabel = new JLabel("<html>Choose At Least 2 Companies <br /> to compare:</html>");
		JScrollPane scrollPane;
		
		instructionLabel.setForeground(Color.RED);
		
		oFrame = new JFrame();
		oFrame.getContentPane().setBackground(new Color(51, 51, 51));
		oFrame.setBounds(100, 100, 1609, 1057);
		oFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		oFrame.getContentPane().setLayout(null);
		oFrame.setTitle("Competitor Analysis");
		
		oRightPanel = new JPanel();
		oRightPanel.setBackground(new Color(51, 51, 51));
		oRightPanel.setBounds(1270, 52, 254, 1040);
		oFrame.getContentPane().add(oRightPanel);
		oRightPanel.setLayout(new BoxLayout(oRightPanel, BoxLayout.Y_AXIS));
		oRightPanel.setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));
		oRightPanel.add(instructionLabel);
		oRightPanel.add(Box.createRigidArea(new Dimension(0, 20)));
		instructionLabel.setFont(new Font("Tahoma", Font.PLAIN, 12));
		
		oTopPanel = new JPanel();
		oTopPanel.setBackground(new Color(51, 51, 51));
		oTopPanel.setBounds(10, 52, 1250, 89);
		oFrame.getContentPane().add(oTopPanel);
		oTopPanel.setLayout(null);
		
		oCompanyProgress = new JProgressBar();
		oCompanyProgress.setBounds(94, 21, 1063, 17);
		oCompanyProgress.setValue(0);
		oCompanyProgress.setMinimum(0);
		oCompanyProgress.setStringPainted(true);
		oTopPanel.add(oCompanyProgress);
		
		oFullProgress = new JProgressBar();
		oFullProgress.setBounds(94, 60, 1063, 17);
		oFullProgress.setStringPainted(true);
		oFullProgress.setValue(0);
		oTopPanel.add(oFullProgress);
		
		overallStatus = new JLabel("");
		overallStatus.setBounds(94, 45, 1150, 14);
		overallStatus.setForeground(new Color(237, 237, 237));
		oTopPanel.add(overallStatus);
		
		oContentTable = new JTable();
		oContentTable.setBounds(0, 0, 706, -605);
		oFrame.getContentPane().add(oContentTable);
		oContentTable.setRowSelectionAllowed(true);
		oContentTable.setAutoCreateRowSorter(true);
		oContentTable.setFillsViewportHeight(true);
		oContentTable.setBackground(new Color(51, 51, 51));
		oContentTable.setForeground(new Color(237, 237, 237));
		oContentTable.setDefaultRenderer(Object.class, myTableRenderer);
		
		scrollPane = new JScrollPane(oContentTable);
		scrollPane.setBounds(0, 0, 706, -605);;
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(51, 51, 51));
		panel.setBounds(10, 187, 1250, 805);
		oFrame.getContentPane().add(panel);
		panel.setLayout(new BorderLayout(0, 0));
		panel.add(scrollPane);
		
		companyStatus = new JLabel("");
		companyStatus.setBounds(94, 5, 1150, 14);
		companyStatus.setForeground(new Color(237, 237, 237));
		oTopPanel.add(companyStatus);
		
		JPanel midPanel = new JPanel();
		midPanel.setBounds(10, 142, 1250, 47);
		midPanel.setBackground(new Color(51, 51, 51));
		oFrame.getContentPane().add(midPanel);
		midPanel.setLayout(null);
		
		itemFoundLbl = new JLabel("0 item(s) found.");
		itemFoundLbl.setBounds(0, 11, 197, 24);
		itemFoundLbl.setForeground(new Color(237, 237, 237));
		itemFoundLbl.setText("");
		midPanel.add(itemFoundLbl);
		
		dataDateLbl = new JLabel("");
		dataDateLbl.setForeground(new Color(237, 237, 237));
		dataDateLbl.setBounds(1026, 11, 197, 24);
		midPanel.add(dataDateLbl);
		
		chckbxHideNonblProducts = new JCheckBox("Hide Non-BL Products");
		chckbxHideNonblProducts.setBounds(214, 7, 161, 33);
		midPanel.add(chckbxHideNonblProducts);
		chckbxHideNonblProducts.setBackground(new Color(51, 51, 51));
		chckbxHideNonblProducts.setForeground(new Color(237, 237, 237));
		
		btnBackToMainMenu = new JButton("Back To Main Menu");
		btnBackToMainMenu.setBounds(20, 10, 40, 40);
		btnBackToMainMenu.setOpaque(false);
		btnBackToMainMenu.setContentAreaFilled(false);
		btnBackToMainMenu.setBorderPainted(false);
		try {
			Image backImg = ImageIO.read(getClass().getResource("/resources/back-arrow.png"));
			Image backIcon = backImg.getScaledInstance(btnBackToMainMenu.getWidth(), btnBackToMainMenu.getHeight(), Image.SCALE_SMOOTH);
			btnBackToMainMenu.setIcon(new ImageIcon(backIcon));
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		btnBackToMainMenu.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				MainMenuGui mainMenu = new MainMenuGui();
				mainMenu.getFrame().setVisible(true);
				oFrame.setVisible(false);
			}
		});
		oFrame.getContentPane().add(btnBackToMainMenu);
		
		chckbxHideNonblProducts.setVisible(false);
		chckbxHideNonblProducts.addItemListener(new ItemListener() {

			@Override
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED)
				{
					command.hideNonBlProducts(myTableRenderer);
				}
				else
				{
					command.displayAllProducts();
				}
			}
			
		});
	}
	
	private void addCompareButton()
	{
		oCompareButton = new JButton("Compare");
		oCompareButton.setBackground(new Color(73, 73, 73));
		oCompareButton.setForeground(new Color(237, 237, 237));
		oCompareButton.setFont(new Font("Tahoma", Font.BOLD, 14));
		oCompareButton.setSize(500, 200);
		
		final JButton tempCompare = oCompareButton;
		oCompareButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				System.out.println("Compare button pressed.");
				command = new Command(oFrame);
				command.execute();
				tempCompare.setEnabled(false);
				oCancelButton.setEnabled(true);
				oSaveToFileButton.setEnabled(true);
			}
		});
		
		oRightPanel.add(oCompareButton);
		oRightPanel.revalidate();
	}
	
	private void addCancelButton()
	{
		oRightPanel.add(Box.createRigidArea(new Dimension(0, 5)));
		oCancelButton = new JButton("Cancel");
		oCancelButton.setBackground(new Color(73, 73, 73));
		oCancelButton.setForeground(new Color(237, 237, 237));
		oCancelButton.setFont(new Font("Tahoma", Font.BOLD, 14));
		oCancelButton.setSize(500, 200);
		oCancelButton.setEnabled(false);
		
		
		oCancelButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				System.out.println("cancel button is clicked");
				command.cancel();
				
				System.out.println("resetting progress bar values");
				setStatusText(oCompanyProgress, 0);
				setStatusText(oFullProgress, 0);
				command.displayAllProducts();
				
				System.out.println("enabling compare button");
				oCompareButton.setEnabled(true);
				
				System.out.println("removing cancel button");
				oCancelButton.setEnabled(false);
			}
		});
		oRightPanel.add(oCancelButton);
		oRightPanel.revalidate();
	}
	
	private void addSaveToFileButton()
	{
		oRightPanel.add(Box.createRigidArea(new Dimension(0, 5)));
		oSaveToFileButton = new JButton("Save to File");
		oSaveToFileButton.setBackground(new Color(73, 73, 73));
		oSaveToFileButton.setForeground(new Color(237, 237, 237));
		oSaveToFileButton.setFont(new Font("Tahoma", Font.BOLD, 14));
		oSaveToFileButton.setSize(500, 200);
		oSaveToFileButton.setEnabled(false);
		
		oSaveToFileButton.addActionListener(new ActionListener()
		{
			
			@Override
			public void actionPerformed(ActionEvent e)
			{
				// TODO Auto-generated method stub
				JFileChooser fileChooser = new JFileChooser();
				int rVal = fileChooser.showSaveDialog(oFrame);
				if (rVal == JFileChooser.APPROVE_OPTION)
				{
					File f = fileChooser.getSelectedFile();
					String fileName = f.getAbsolutePath();
					command.saveData(fileName);
				}
			}
		});
		
		oRightPanel.add(oSaveToFileButton);
		oRightPanel.revalidate();
	}
	
	private void setStatusText(final JProgressBar progressBar, final int status)
	{
        SwingUtilities.invokeLater(new Runnable()
        {
 
            //@Override
            public void run()
            {
                progressBar.setValue(status);
            }
 
        });
    }

	/**
	 * Create a check box for every company module found in folder.
	 * @param text The name of the company.
	 */
	public static void addCheckBox(String text)
	{
		System.out.println("adding check box for " + text);
		JCheckBox checkbox = new JCheckBox(text);
		checkbox.setBackground(new Color(51, 51, 51));
		checkbox.setForeground(new Color(237, 237, 237));
		
		
		if (text.contains("BullionList"))
		{
			checkbox.setSelected(true); // by default, always have BL selected
		}
		oRightPanel.add(checkbox);
		oRightPanel.add(Box.createRigidArea(new Dimension(0, 5)));
		oRightPanel.revalidate();
	}
	
	public static JPanel getRightPanel()
	{
		return oRightPanel;
	}
	
	public static JTable getContentTable()
	{
		return oContentTable;
	}
	
	public static JProgressBar getCompanyProgressBar()
	{
		return oCompanyProgress;
	}
	
	public static JProgressBar getFullProgressBar()
	{
		return oFullProgress;
	}
	
	public static JLabel getOverallStatus()
	{
		return overallStatus;
	}

	public static JLabel getCompanyStatus()
	{
		return companyStatus;
	}
	
	public static JLabel getDataDateLabel()
	{
		return dataDateLbl;
	}
	
	public static JCheckBox getHidingCheckbox()
	{
		return chckbxHideNonblProducts;
	}
	
	public static void enableCompareButton()
	{
		oCompareButton.setEnabled(true);
	}
	
	public static JLabel getItemFoundLabel()
	{
		return itemFoundLbl;
	}
	
	public static void disableCancelButton()
	{
		oCancelButton.setEnabled(false);
	}
}
