package tests;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import javax.swing.JFrame;

import plugins.ABCAnalysis;
import plugins.AJMBullionAnalysis;
import plugins.APMEXAnalysis;
import plugins.BullionDepositAnalysis;
import plugins.BullionListAnalysis;
import plugins.BullionMoneyAnalysis;
import plugins.GoldStackersAnalysis;
import plugins.PerthBullionAnalysis;
import resources.Product;
import misc.Command;
import misc.CurrencyConverter;
import misc.PluginFinder;

public class Test {
	private static Map<Product, ArrayList<String>> productPrices;
	
	public static void main(String args[]) {
//		ABCAnalysis abc = new ABCAnalysis();
//		BullionListAnalysis bl = new BullionListAnalysis();
//		PluginFinder pFinder = new PluginFinder();
//		BullionMoneyAnalysis bm = new BullionMoneyAnalysis();
//		GoldStackersAnalysis gs = new GoldStackersAnalysis();
//		APMEXAnalysis apmex = new APMEXAnalysis();
//		PerthBullionAnalysis pb = new PerthBullionAnalysis();
//		AJMBullionAnalysis ajm = new AJMBullionAnalysis();
//		System.out.println(Character.CURRENCY_SYMBOL);
//		CurrencyConverter cc = new CurrencyConverter();
		BullionDepositAnalysis bd = new BullionDepositAnalysis();
		
		System.out.println("Getting contents");
//		abc.getProducts();
//		System.out.println();
//		bl.getProducts();
//		pFinder.find();
		productPrices = new HashMap<Product, ArrayList<String>>();
//		bm.getProducts();
//		pb.getProducts();
		
//		System.out.println("rate: " + cc.getUSDAUDRate());
		
//		gs.getProducts();
		
//		apmex.getProducts();
//		ajm.getProducts();
		bd.getProducts();
		
//		Command comm = new Command(new JFrame());
//		comm.loadExistingData();
		
//		Product a = new Product("1oz PAMP Gold Minted Fortuna", "1400");
//		Product b = new Product("1oz PAMP Cast Gold", "1400");
////		addProductToList(a);
//		addProductToList(b);
		
//		System.out.print(a.equals(b));
//		System.out.println(": " + a.hashCode() + ", " + b.hashCode());
//		System.out.println();
		
//		a = new Product("1oz PAMP Gold Minted Fortuna", "1400");
//		b = new Product("1oz PAMP Gold Fortuna", "1400");
//		addProductToList(a);
//		addProductToList(b);
//		System.out.print(a.equals(b));
//		System.out.println(": " + a.hashCode() + ", " + b.hashCode());
//		System.out.println();
		
//		a = new Product("1/2 oz Lunar Horse Silver 2014", "700");
//		b = new Product("1/2 oz Lunar Goat Silver 2015", "700");
//		addProductToList(a);
//		addProductToList(b);
//		System.out.print(a.equals(b));
//		System.out.println(": " + a.hashCode() + ", " + b.hashCode());
//		System.out.println();
		
//		a = new Product("1/2 oz Kookaburra Silver 2014", "700");
//		b = new Product("1/2 oz Koala Silver 2014", "700");
//		addProductToList(a);
//		addProductToList(b);
//		System.out.print(a.equals(b));
//		System.out.println(": " + a.hashCode() + ", " + b.hashCode());
//		System.out.println();
		
//		a = new Product("1/20 oz Gold 2014 Lunar Goat", "700");
//		b = new Product("1/20 oz Gold Perth Mint Lunar Goat Minted Coin", "700");
//		addProductToList(a);
//		addProductToList(b);
//		System.out.print(a.equals(b));
//		System.out.println(": " + a.hashCode() + ", " + b.hashCode());
//		System.out.println();
//		
//		a = new Product("2014 Lunar Horse 1/10oz Gold Coin", "185.50");
//		b = new Product("1/10 oz Lunar Horse Gold 2014", "183.52");
//		addProductToList(a);
//		addProductToList(b);
//		System.out.println(a.getYear());
//		System.out.print(a.equals(b));
//		System.out.println(": " + a.hashCode() + ", " + b.hashCode());
//		System.out.println();
		
//		a = new Product("1/2 oz Gold 2015 Perth Mint Kangaroo Coin", "700");
//		b = new Product("1/2 oz Gold Perth Mint Kangaroo Minted Coin", "700");
//		addProductToList(a);
//		addProductToList(b);
//		System.out.print(a.equals(b));
//		System.out.println(": " + a.hashCode() + ", " + b.hashCode());
//		System.out.println();
		
//		a = new Product("1/10oz Gold 2015 Goat Lunar Coin", "108");
//		b = new Product("1/10oz Gold Perth Mint Lunar Goat Minted Coin", "108");
//		addProductToList(a);
//		addProductToList(b);
//		System.out.print(a.equals(b));
//		System.out.println(": " + a.hashCode() + ", " + b.hashCode());
//		System.out.println();
		
//		a = new Product("1/10oz Gold 2015 Perth Mint Kangaroo Coin", "108");
//		b = new Product("1/10oz Gold Perth Mint Kangaroo Minted Coin", "108");
//		addProductToList(a);
//		addProductToList(b);
//		System.out.print(a.equals(b));
//		System.out.println(": " + a.hashCode() + ", " + b.hashCode());
//		System.out.println();
		
//		a = new Product("1 oz Southern Cross Cast Button Silver", "26");
//		b = new Product("1 oz Silver Southern Cross Button Cast", "26");
//		addProductToList(a);
//		addProductToList(b);
//		System.out.print(a.equals(b));
//		System.out.println(": " + a.hashCode() + ", " + b.hashCode());
//		System.out.println();
		
//		printMap();
	}
	
	private static void addProductToList(Product product)
	{
		if (productPrices.containsKey(product) == true) // has product in list
		{
			System.out.println("Product: " + product.getFullName() + " is already in the list.");
			ArrayList<String> prices = productPrices.get(product);
			prices.add(product.getPrice());
		}
		else // has no product in list
		{
			ArrayList<String> prices = new ArrayList<String>();
			prices.add(product.getPrice());
			productPrices.put(product, prices);
		}
	}
	
	private static void printMap()
	{
		Iterator<Entry<Product, ArrayList<String>>> it = productPrices.entrySet().iterator();
		while (it.hasNext())
		{
			Map.Entry<Product, ArrayList<String>> pairs = it.next();
			System.out.print(pairs.getKey().getFullName() + ": ");
			for (String p : pairs.getValue())
			{
				System.out.print(p + ", ");
			}
			System.out.println();
		}
	}
}
