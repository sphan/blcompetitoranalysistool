package plugins;

import gui.RetailGui;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;

import misc.Command;
import resources.Product;

public abstract class CompetitorAnalysis extends SwingWorker<Object, Object>
{
	private static final int TIME_OUT = 5000;
	private static final String USER_AGENT = "Chrome";
	
	private Command command;
	private boolean isCompleted = false;
	
	protected List<Product> productList;
	
	public abstract List<Product> getProducts();
	
	public List<Product> getProductList()
	{
		return productList;
	}
	
	public void cancel()
	{
		this.cancel(true);
	}
	
	public Command getCommand()
	{
		return command;
	}
	
	public void setPluginStatusComplete()
	{
		this.isCompleted = true;
	}
	
	public boolean isPluginCompleted()
	{
		return isCompleted;
	}

	public void setCommand(Command command) {
		this.command = command;
	}
	
	protected String getPageContent(String url) {
		String contents = "";
		String line;
		BufferedReader br;
		
		try
		{
			
			url = addProtocol(url);
//			System.out.println(url);
			HttpURLConnection httpConn = connectURL(new URL(url));
			HttpURLConnection.setFollowRedirects(true);
			br = new BufferedReader(new InputStreamReader(httpConn.getInputStream()));
			
			while (((line = br.readLine()) != null) && (this.isCancelled() == false))
			{
				contents += line;
				contents += "\n";
			}
			
			br.close();
			httpConn.disconnect();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		
		return contents;
	}
	
	protected String extractContent(String line, String regex)
	{
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(line);
		String price = "";
		
		while (matcher.find() == true)
		{
			price = matcher.group(1);
		}
		
		return price;
	}
	
	protected void setStatusText(final int status)
	{
        SwingUtilities.invokeLater(new Runnable()
        {
 
            //@Override
            public void run()
            {
 
                RetailGui.getCompanyProgressBar().setValue(status);
            }
 
        });
    }
	
	@Override
	protected Object doInBackground() throws Exception
	{
		setStatusText(0);
		getProducts();
		return null;
	}
	
	protected void done()
	{ 
        setStatusText(100);
    }
	
	protected boolean containsRegex(String line, String regex)
	{
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(line);
		boolean matched = true;
		
		if (matcher.find() == false)
		{
			matched = false;
		}
		
		return matched;
		
	}
	
	protected void setCompanyLoadingStatus(String message)
	{
		command.setCompanyLoadingStatus(message);
	}
	
	protected void setOverallLoadingStatus(String message)
	{
		command.setOverallLoadingStatus(message);
	}
	
	private String addProtocol(String url) {
		if (hasProtocol(url) == false)
		{
			url = "https://" + url;
		}
		
		return url;
	}
	
	private boolean hasProtocol(String url) {
//		String protocol = url.substring(0, 3);
		boolean hasProtocol = false;
//		System.out.println("url: " + url);
		
		if (url.contains("http") == true || url.contains("https") == true)
		{
			hasProtocol = true;
		}
		
		return hasProtocol;
	}
	
	private HttpURLConnection connectURL(URL url) {
		HttpURLConnection httpConn = null;
		try {
			httpConn = (HttpURLConnection) url.openConnection();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		httpConn.setConnectTimeout((int) TIME_OUT);
		httpConn.addRequestProperty("User-Agent", USER_AGENT);
		httpConn.setInstanceFollowRedirects(true);
		
		return httpConn;
	}
}
