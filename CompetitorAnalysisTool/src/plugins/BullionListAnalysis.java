package plugins;

import gui.RetailGui;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import resources.Product;

public class BullionListAnalysis extends CompetitorAnalysis
{
	
	private String oBlUrl;
	private int itemNum; // total number of items
	
	public BullionListAnalysis()
	{
		super();
		productList = new ArrayList<Product>();
		oBlUrl = "www.bullionlist.com.au/browse";
		setPluginStatusComplete();
	}

	@Override
	public List<Product> getProducts()
	{
		setOverallLoadingStatus("Extracting data from Bullion List...");
		setCompanyLoadingStatus("Connecting to Bullion List website...");
		setStatusText(0);
		
		String contents = getPageContent(oBlUrl);
		Scanner scanner = new Scanner(contents);
		String line = "";
		String name = "";
		String price = "";
		boolean gotPrice = false;
		int itemCount = 0;
		
		
		
		
		while ((scanner.hasNextLine() == true) && (this.isCancelled() == false))
		{
			line = scanner.nextLine();
			
			if (line.contains("id") == true && line.contains("ProductCountLabel") == true)
			{
				itemNum = Integer.parseInt(extractContent(line, ">(\\d+)<"));
				RetailGui.getCompanyProgressBar().setMaximum(itemNum);
				System.out.print(itemNum);
			}
			
			if (line.contains("class") == true && line.contains("name") == true &&
			   (line.contains("Silver") == true || line.contains("Gold") == true))
			{
				name = extractName(line);
			}
			
			if (line.contains("class") == true && line.contains("row0") == true)
			{
				while (gotPrice == false && scanner.hasNextLine() == true)
				{
					line = scanner.nextLine();
					if (line.contains("class") == true && line.contains("price") == true)
					{
						price = extractContent(line, ">(.*?)<");
						gotPrice = true;
					}
				}
			}
			
			if (name.isEmpty() == false && price.isEmpty() == false)
			{
				productList.add(new Product(name, price));
				name = "";
				price = "";
				itemCount++;
				setStatusText(itemCount);
				setCompanyLoadingStatus("Loading item " + itemCount + " of approximately " + itemNum + " items...");
			}
			gotPrice = false;
		}
		
		scanner.close();
		
		return productList;
	}
	
	private String extractName(String line)
	{
		Pattern pattern = Pattern.compile("<a.*>\\s*(.*?)\\s*</a>");
		Matcher matcher = pattern.matcher(line);
		String name = "";
		
		while (matcher.find() == true)
		{
			name = matcher.group(1);
		}
		
		return name;
	}
}
