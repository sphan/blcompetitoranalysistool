package plugins;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import resources.Product;

public class PerthBullionAnalysis extends CompetitorAnalysis
{
	
	private String pbURL;
	
	public PerthBullionAnalysis()
	{
		super();
		pbURL = "http://www.perthbullion.com/shop/";
		productList = new ArrayList<Product>();
		setPluginStatusComplete();
	}

	@Override
	public List<Product> getProducts()
	{
		setOverallLoadingStatus("Extracting data from Perth Bullion...");
		setCompanyLoadingStatus("Connecting to Perth Bullion website...");
		setStatusText(0);
		
		// TODO Auto-generated method stub
		String contents = getPageContent(pbURL);
		Scanner scanner = null;
		String line = "";
		String name = "";
		String price = "";
		int pageCount = 1;
		int itemCount = 0;
		int totalItemNum = 0;
		boolean doneCategory = false;
		boolean foundSomeProduct = false;
		List<String> categories = getCategoryURLs(contents);
		
		for (String category : categories)
		{
			contents = getPageContent(category + "page/" + pageCount);
			scanner = new Scanner(contents);
			System.out.println(category + "page/" + pageCount);
			while (doneCategory == false)
			{
				while (scanner.hasNextLine() == true)
				{
					line = scanner.nextLine();
//					if ((line.contains("id") == true) && (line.contains("products_table")))
//					{
//						System.out.println(line);
					while ((scanner.hasNextLine() == true) && (line.contains("gridProductTitle") == false))
					{
						line = scanner.nextLine();
//							System.out.println(line);
					}
//						System.out.println(line);
					
					while ((scanner.hasNextLine() == true) && (line.contains("href") == false))
					{
						line = scanner.nextLine();
					}
//						System.out.println(line);
					name = extractContent(line, ">(.*?)<");
					
					while ((scanner.hasNextLine() == true) && (line.contains("gridProductPrice") == false))
					{
						line = scanner.nextLine();
					}
					if (scanner.hasNextLine())
					{
						line = scanner.nextLine();
					}
					
					if (scanner.hasNextLine())
					{
						line = scanner.nextLine();
					}
					System.out.println(line);
					price = reformatPrice(line);
//					price = line.replaceAll(" *", "");
//							Charset.forName("UTF-8").encode(extractContent(line, ">(.*?)<")).toString();
					
//					System.out.println(name + ": " + price);
					System.out.println(price);
					
					if (foundSomeProduct == false && (scanner.hasNextLine() == false))
					{
						doneCategory = true;
					}
					else
					{
//							name = extractContent(line, ">(.*?)<");
						foundSomeProduct = true;
//						System.out.println(name);
					}
					
					if (name.isEmpty() == false && price.isEmpty() == false)
					{
						Product p = new Product(name, price);
						if (productList.contains(p) == false)
						{
							productList.add(p);
//							System.out.println(p.getFullName() + ": " + p.getPrice());
						}
						line = "";
						price = "";
						itemCount++;
						totalItemNum += itemCount;
					}
						
//					}
				}
				if (doneCategory == false)
				{
					pageCount++;
					setCompanyLoadingStatus("Loading item " + itemCount + " of approximately " + totalItemNum + " items...");
					contents = getPageContent(category + "page/" + pageCount);
					scanner = new Scanner(contents);
					System.out.println(foundSomeProduct);
					System.out.println(doneCategory);
					System.out.println(category + "page/" + pageCount);
				}
				foundSomeProduct = false;
			}
			doneCategory = false;
			pageCount = 1;
		}
		scanner.close();
		
		return productList;
	}
	
	private String reformatPrice(String oldPrice)
	{
		String newPrice = "";
		oldPrice = oldPrice.replaceAll("\\s*", "");
		String[] nums = oldPrice.split("&#");
		
		for (String n : nums)
		{
//			System.out.println(n);
			if (n.length() < 2 || n.contains("html"))
			{
				continue;
			}
			
			String val = n.substring(0, 2);
			int intVal = Integer.parseInt(val) - 48;
			
			newPrice += intVal;
			
			if (n.contains(".") == true)
			{
				newPrice += ".";
			}
			
			if (n.contains("-") == true)
			{
				newPrice += "-";
			}
			
//			System.out.println(n);
		}
//		System.out.println("newPrice after removing spaces: " + oldPrice);
//		System.out.println(newPrice);
		
//		Pattern pattern = Pattern.compile("$(&#d+)*.(&#d+)");
//		Matcher matcher = pattern.matcher(oldPrice);
//		
//		int i = 1;
//		System.out.println(matcher.groupCount());
//		while (matcher.find() == true)
//		{
//			String value = matcher.group(i);
//			System.out.println("value: " + value);
//			value.replace("&#", "");
//			int intValue = Integer.parseInt(value);
//			newPrice += intValue;
//			i++;
//		}
		
		String[] prices = newPrice.split("-");
		float biggerPrice = 0;
		for (String p : prices)
		{
			if (p.isEmpty() == true)
			{
				continue;
			}
			float price = Float.parseFloat(p);
			if (price > biggerPrice)
			{
				biggerPrice = price;
			}
		}
		
		return String.valueOf(biggerPrice);
	}

	private List<String> getCategoryURLs(String contents)
	{
		List<String> categories = new ArrayList<String>();
		Scanner scanner = new Scanner(contents);
		String line = "";
		String url = "";
		
		while (scanner.hasNextLine() == true)
		{
			line = scanner.nextLine();
			
			if ((line.contains("class") == true) && (line.contains("categoryItem") == true))
			{
				while (line.contains("href") == false)
				{
					line = scanner.nextLine();
				}
				if (containsRegex(line, "gold|silver|other"))
				{
					url = extractContent(line, "href=\"\\s*(.*?)\\s*\"");
					categories.add(url);
//					System.out.println(url);
				}
				
			}
		}
		
		return categories;
	}
}
