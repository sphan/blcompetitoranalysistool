package plugins;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import resources.Product;

public class AJMBullionAnalysis extends CompetitorAnalysis
{

	private String ajmURL;
	private int itemNum;
	
	public AJMBullionAnalysis()
	{
		super();
		productList = new ArrayList<Product>();
		ajmURL = "http://ajmbullion.com.au";
		itemNum = 0;
	}
	
	@Override
	public List<Product> getProducts() {
		setCompanyLoadingStatus("Connecting to AJM Bullion...");
		setOverallLoadingStatus("Extracting data from AJM Bullion...");
		setStatusText(0);
		
		String contents = getPageContent(ajmURL);
		Scanner scanner = null;
		String line = "";
		String name = "";
		String price = "";
		int itemCount = 0;
		List<String> categories = getCategoryURLs(contents);
		
		
		
		for (String category : categories)
		{
			contents = getPageContent(category + "?product_count=45");
			scanner = new Scanner(contents);
			
			while (scanner.hasNextLine() == true  && this.isCancelled() == false)
			{
				line = scanner.nextLine();
				if ((line.contains("class") == true) && (line.contains("product-title")))
				{
					while (line.contains("href") == false)
					{
						line = scanner.nextLine();
					}
					name = extractContent(line, "><.*?>(.*?)</a>");
					name = name.replace("&#8211;", "-");
					name = name.replace("&#8216;", "'");
					name = name.replace("&#8217;", "'");
					name = name.replace("&# ;", "'");
				}
				
				if ((line.contains("class")) && (line.contains("price") == true))
				{
					while (line.contains("amount") == false)
					{
						line = scanner.nextLine();
					}
					price = extractContent(line, "><.*?>(.*?)<");
					price = price.replace("&#36;", "$");
					price = price.replace("&nbsp;", "");
				}
				
				if (name.isEmpty() == false && price.isEmpty() == false)
				{
					Product product = new Product(name, price);
					if (productList.contains(product) == false)
					{
						productList.add(product);
						System.out.println(product.getFullName() + ": " + product.getPrice());
						itemNum++;
						itemCount++;
					}
					name = "";
					price = "";
					setCompanyLoadingStatus("Reading item " + itemCount + " of approximately " + itemNum + " items...");
					setStatusText(itemCount);
				}
			}
		}
		scanner.close();
		
		return productList;
	}
	
	private List<String> getCategoryURLs(String contents)
	{
		List<String> categories = new ArrayList<String>();
		Scanner scanner = new Scanner(contents);
		String line = "";
		String url = "";
		
		while (scanner.hasNextLine() == true)
		{
			line = scanner.nextLine();
			
			if ((line.contains("id") == true) && (containsRegex(line, "menu-item-\\d+") == true) && (line.contains("has-children") == false))
			{
				while (line.contains("href") == false)
				{
					line = scanner.nextLine();
				}
				url = extractContent(line, "href=\"\\s*(.*?)\\s*\"");
				categories.add(url);
			}
		}
		
		return categories;
	}

}
