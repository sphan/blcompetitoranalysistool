package plugins;

import gui.RetailGui;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import resources.Product;

public class BullionMoneyAnalysis extends CompetitorAnalysis
{
	private String bmURL;
	private static final int ITEM_NUM = 201;
	
	public BullionMoneyAnalysis()
	{
		bmURL = "www.bullionmoney.com.au/products";
		productList = new ArrayList<Product>();
		RetailGui.getCompanyProgressBar().setMaximum(ITEM_NUM);
		setPluginStatusComplete();
	}

	@Override
	public List<Product> getProducts()
	{
		System.out.println("Getting product list from Bullion Money.");
		setOverallLoadingStatus("Extracting data from Bullion Money...");
		setCompanyLoadingStatus("Connecting to Bullion Money website...");
		setStatusText(0);
		
		String contents = getPageContent(bmURL);
		Scanner scanner = new Scanner(contents);
		String line = "";
		String name = "";
		String price = "";
		int itemCount = 0;
		
		
		List<String> categories = getCategoryURLs(contents, scanner);
		
		for (String category : categories)
		{
			contents = getPageContent(category);
			scanner = new Scanner(contents);
			
			while (scanner.hasNextLine() == true && this.isCancelled() == false)
			{
				line = scanner.nextLine();
				if (line.contains("name") == true && line.contains("item") == true)
				{
					while (line.contains("<h1>") == false)
					{
						line = scanner.nextLine();
					}
					
					name = extractContent(line, "<h1>\\s*(.*?)\\s*</h1>");
					
					while (containsRegex(line, "price-\\d+") == false)
					{
						line = scanner.nextLine();
					}
					
					price = extractContent(line, ">\\s*AUD\\s*(.*?)\\s*<");
					if (name.isEmpty() == false && price.isEmpty() == false)
					{
						productList.add(new Product(name, price));
						name = "";
						price = "";
						itemCount++;
						setStatusText(itemCount);
						setCompanyLoadingStatus("Loading item " + itemCount + " of approximately " + ITEM_NUM + " items...");
					}
				}
			}
		}
		
		scanner.close();
		return productList;
	}

	private List<String> getCategoryURLs(String contents, Scanner scanner)
	{
		List<String> categories = new ArrayList<String>();
		String line = "";
		String url = "";
		
		while (scanner.hasNextLine() == true)
		{
			line = scanner.nextLine();
			
			if (containsRegex(line, "data-id=\"\\s*\\d+\\s*\""))
			{
				line = scanner.nextLine();
				if (line.contains("href") == true)
				{
					url = extractContent(line, "href=\"\\s*(.*?)\\s*\"");
					categories.add(url);
				}
			}
		}
		
		return categories;
	}
}
