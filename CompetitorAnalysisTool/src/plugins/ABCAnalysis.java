package plugins;

import gui.RetailGui;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import resources.Product;

public class ABCAnalysis extends CompetitorAnalysis {
	
	private String oAbcUrl;
	private int oPageCount;
	private static final int ITEM_NUM = 102;
	
	
	public ABCAnalysis()
	{
		super();
		oAbcUrl = "www.abcbullion.com.au/store/page/";
		oPageCount = 0;
		productList = new ArrayList<Product>();
		RetailGui.getCompanyProgressBar().setMaximum(ITEM_NUM);
		setPluginStatusComplete();
	}

	@Override
	public List<Product> getProducts()
	{
		
		String contents;
		Scanner scanner = null;
		String line = null;
		String name = "";
		String price = "";
		int itemCount = 0;
		boolean finishedReadingPage = false;
		boolean allLoaded = false;
		
		setCompanyLoadingStatus("Connecting to ABC website...");
		setOverallLoadingStatus("Extracting data from ABC...");
		setStatusText(0);
		
		
		while ((allLoaded == false) && (this.isCancelled() == false)) // check if all products has been loaded from all pages
		{
			contents = getPageContent(incrementPage());
			scanner = new Scanner(contents);
			
			while ((finishedReadingPage == false) && (scanner.hasNextLine()) && (this.isCancelled() == false)) 
			{
				line = scanner.nextLine();
				
				if (line.contains("No saleable items found"))
				{
					finishedReadingPage = true;
					allLoaded = true;
				}
				
				if (line.contains("itemprop") == true &&
					line.contains("name") == true)
				{
					line = scanner.nextLine();
					name = removeTag(line);
				}
				if (line.contains("span") == true &&
					line.contains("class") == true &&
					line.contains("price") == true)
				{
					price = extractContent(line, ">(.*?)<");
				}
				
				if (line.contains("id") == true && line.contains("loadmore")) 
				{
					finishedReadingPage = true;
				}
				
				if (name.isEmpty() == false && price.isEmpty() == false) {
					productList.add(new Product(name, price));
					name = "";
					price = "";
					itemCount++;
					setStatusText(itemCount);
					setCompanyLoadingStatus("Reading item " + itemCount + " of approximately " + ITEM_NUM + " items...");
				}
			}
			
			finishedReadingPage = false;
		}
		
		scanner.close();
		return productList;
	}
	
	
	
	private String removeTag(String line)
	{
		Pattern pattern = Pattern.compile("(.*?)\\s+</a>");
		Matcher matcher = pattern.matcher(line);
		String name = "";
		
		while (matcher.find() == true) {
			name = matcher.group(1);
		}
		
		return name;
	}

	private String incrementPage()
	{
		oPageCount++;
		return oAbcUrl + oPageCount;
	}

	
}
