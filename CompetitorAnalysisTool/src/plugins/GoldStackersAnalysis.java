package plugins;

import gui.RetailGui;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import resources.Product;

public class GoldStackersAnalysis extends CompetitorAnalysis
{
	private String gsURL;
	private static final String LIMIT = "?limit=all";
	private int totalItemNum;
	
	public GoldStackersAnalysis()
	{
		gsURL = "http://www.goldstackers.com.au/";
		productList = new ArrayList<Product>();
		totalItemNum = 0;
		setPluginStatusComplete();
	}

	@Override
	public List<Product> getProducts()
	{
		setOverallLoadingStatus("Extracting data from Gold Stackers...");
		setCompanyLoadingStatus("Connecting to Gold Stackers website...");
		setStatusText(0);
		
		String contents = getPageContent(gsURL);
		Scanner scanner = null;
		List<String> categories = extractCategories(contents);
		String line = "";
		String name = "";
		String price = "";
		int itemCount = 0;
		int itemNum;
		
		
		for (String category : categories)
		{
			contents = getPageContent(category + LIMIT); // to display all products in one page.
			scanner = new Scanner(contents);
			itemNum = 0;
			
			while (scanner.hasNextLine() == true && (this.isCancelled() == false))
			{
				line = scanner.nextLine();
				if ((line.contains("class") == true) && (line.contains("product-name") == true))
				{
					while (line.contains("title") == false)
					{
						line = scanner.nextLine();
					}
					name = extractContent(line, "title=\"\\s*(.*?)\\s*\"");
				}
				
				if ((line.contains("id") == true) && (containsRegex(line, "product-price-\\d+")))
				{
					line = scanner.nextLine();
					price = extractContent(line, "price\">\\s*(.*?)\\s*<");
				}
				
				if (name.isEmpty() == false && price.isEmpty() == false)
				{
					Product product = new Product(name, price);
					productList.add(product);
					name = "";
					price = "";
					itemCount++;
					setStatusText(itemCount);
					setCompanyLoadingStatus("Loading item " + itemCount + " of approximately " + totalItemNum + " items...");
				}
				
				if (line.contains("class") == true && line.contains("amount") == true && itemNum == 0)
				{
					line = scanner.nextLine();
					itemNum = Integer.parseInt(extractContent(line, "(\\d+)\\s*Item"));
					totalItemNum += itemNum;
				}
			}
		}
		
		scanner.close();
		return productList;
	}
	
	private List<String> extractCategories(String contents)
	{
		System.out.println("extracting categories");
		List<String> categories = new ArrayList<String>();
		Scanner scanner = new Scanner(contents);
		String line = "";
		String url = "";
		
		while (scanner.hasNextLine())
		{
			line = scanner.nextLine();
			if ((line.contains("class") == true) && (line.contains("level0") == true))
			{
				while (line.contains("href") == false)
				{
					line = scanner.nextLine();
				}
				url = extractContent(line, "href=\"\\s*(.*?)\\s*\"");
				categories.add(url);
			}
		}
		scanner.close();
		
		return categories;
	}
}
