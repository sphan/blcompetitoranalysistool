package plugins;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import resources.Product;

public class APMEXAnalysis extends CompetitorAnalysis
{
	private static final String apmexURL = "http://www.apmex.com";
	
	public APMEXAnalysis() {
		// TODO Auto-generated constructor stub
		super();
		productList = new ArrayList<Product>();
	}

	@Override
	public List<Product> getProducts()
	{
		String content = getPageContent(apmexURL);
		List<String> categories = getCategoryURLS(content);
		List<String> categoryItems;
		int pageCount = 0;
		
		for (String category : categories)
		{
			
			content = getPageContent(apmexURL + category);
			categoryItems = getCategoryItemURLS(content);
			
			for (String categoryItem : categoryItems)
			{
				pageCount++;
				System.out.println(categoryItem);
				content = getPageContent(apmexURL + categoryItem + "/all?page=" + pageCount + "&ipp=80");
				System.out.println(apmexURL + categoryItem + "/all?page=" + pageCount + "&ipp=80");
//				getProductItems(content);
			}
		}
		
		return productList;
	}
	
	public void getProductItems(String content)
	{
		Scanner scanner = new Scanner(content);
		String line = "";
		String name = "";
		String price = "";
		
		while (scanner.hasNextLine() == true)
		{
			line = scanner.nextLine();
			if ((line.contains("class") == true) && (line.contains("product-item")))
			{
				while ((line.contains("product-item-title") == false) && (scanner.hasNextLine() == true))
				{
					line = scanner.nextLine();
				}
				
				while ((line.contains("href") == false) && (scanner.hasNextLine() == true))
				{
					line = scanner.nextLine();
					System.out.println(line);
				}
//				name = extractContent(line, ">(.*?)<");
//				System.out.println(name);
			}
		}
		
		scanner.close();
	}
	
	private List<String> getCategoryURLS(String content)
	{
		List<String> categories = new ArrayList<String>();
		Scanner scanner = new Scanner(content);
		String line = "";
		String url = "";
		
		while (scanner.hasNextLine())
		{
			line = scanner.nextLine();
			if ((line.contains("class") == true) && (line.contains("dropdown") == true) && (line.contains("full-width") == true) &&
				(line.contains("Gold") == true) || (line.contains("Silver") == true) || (line.contains("Platinum") == true) || (line.contains("Palldadium") == true))
			{
				while (line.contains("href") == false)
				{
					line = scanner.nextLine();
				}
				url = extractContent(line, "href=\"\\s*(.*?)\\s*\"");
				categories.add(url);
			}
		}
		scanner.close();
		
		return categories;
	}
	
	private List<String> getCategoryItemURLS(String content)
	{
		List<String> items = new ArrayList<String>();
		Scanner scanner = new Scanner(content);
		String line = "";
		String url = "";
		
		while (scanner.hasNextLine())
		{
			line = scanner.nextLine();
			if ((line.contains("class") == true) && (line.contains("category-item") == true))
			{
				while (line.contains("href") == false)
				{
					line = scanner.nextLine();
				}
				if (line.contains("category") == true)
				{
					url = extractContent(line, "href=\"\\s*(.*?)\\s*\"");
					items.add(url);
//					System.out.println("\t" + url);
				}
				
			}
		}
		scanner.close();
		
		return items;
	}

}
