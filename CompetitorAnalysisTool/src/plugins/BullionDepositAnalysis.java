package plugins;

import gui.RetailGui;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import resources.Product;

public class BullionDepositAnalysis  extends CompetitorAnalysis
{

	private String bdURL;
	
	public BullionDepositAnalysis()
	{
		super();
		productList = new ArrayList<Product>();
		bdURL = "www.bulliondeposit.com.au/shop/?products-per-page=all";
		setPluginStatusComplete();
	}

	@Override
	public List<Product> getProducts() {
		String content = getPageContent(bdURL);
//		System.out.println("content: " + content);
		Scanner scanner = new Scanner(content);
		String line = "";
		String name = "";
		String price = "";
		int itemNum = 0, totalItemNum = 0;
		
		setOverallLoadingStatus("Extracting data from Bullion Deposit Australia...");
		setCompanyLoadingStatus("Connecting to Bullion Deposit Australia website...");
		setStatusText(0);
		
		while (scanner.hasNextLine() == true)
		{
			line = scanner.nextLine();
//			System.out.println("line: " + line);
			
			if (line.contains("class") == true && line.contains("product-name") == true)
			{
//				System.out.println("line: " + line);
				name = extractContent(line, "<a.*?>(.*?)</a>");
				System.out.println("name: " + name);
			}
			if (line.contains("class") == true && line.contains("amount") == true)
			{
//				System.out.println("line: " + line);
				price = extractContent(line, "amount\">.*?nbsp;(.*?)</span>");
				System.out.println("price: " + price);
			}
			
			if (name.isEmpty() == false && price.isEmpty() == false)
			{
				productList.add(new Product(name, price));
				name = "";
				price = "";
				totalItemNum++;
				itemNum++;
				setStatusText(itemNum);
				RetailGui.getCompanyProgressBar().setMaximum(totalItemNum);
				setCompanyLoadingStatus("Loading item " + itemNum + " of approximately " + totalItemNum + " items...");
			}
		}
		
		scanner.close();
		
		return productList;
	}
}
